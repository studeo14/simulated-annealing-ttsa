#!/usr/bin/env bash
t=400
b=0.9999
w=4000
d=1.04
th=1.04
maxc=5000
maxp=7100
maxr=10
cmd="cargo run --release -- \
      -t $t\
      -b $b\
      -d $d\
      -w $w\
      -c $maxc\
      -p $maxp\
      -r $maxr\
      -l $th\
      $@\
      "
$cmd
