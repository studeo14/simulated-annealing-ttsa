use std::fmt;
use std::cmp::Ordering;
use colored::*;

const DEFAULT_GAME: Game = Game {
    val: 0,
    game_type: GameType::Home,
};

#[derive(Copy, Clone, Debug, Ord, Eq, PartialEq, PartialOrd, Hash)]
pub enum GameType {
    Home,
    Away,
}

impl GameType {
    pub fn opposite(o: GameType) -> GameType {
        match o {
            GameType::Away => GameType::Home,
            GameType::Home => GameType::Away,
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, Hash)]
pub struct Game {
    pub val: usize,
    pub game_type: GameType,
}

impl Ord for Game {
    fn cmp(&self, other: &Game) -> Ordering {
        match self.val.cmp(&other.val) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => self.game_type.cmp(&other.game_type),
        }
    }
}

impl PartialOrd for Game {
    fn partial_cmp(&self, other: &Game) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Game {
    fn eq(&self, other: &Game) -> bool {
        self.val == other.val && self.game_type == other.game_type
    }
}

impl Game {
    pub fn new(val: usize, game_type: GameType) -> Game {
        Game { val, game_type }
    }
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let printable = match self.game_type {
            GameType::Away => format!("-{}", self.val),
            GameType::Home => format!("{}", self.val),
        };
        let printable = if *self == DEFAULT_GAME {
            printable.red().bold()
        } else {
            printable.normal()
        };
        if let Some(width) = f.width() {
            write!(f, "{:<width$}", printable, width = width)
        } else {
            write!(f, "{}", printable)
        }
    }
}
