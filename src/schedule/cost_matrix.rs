use std::fs;

const MAX_SIZE: usize = 16;
type Mat = [[usize; MAX_SIZE]; MAX_SIZE];

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CostMatrix {
    costs: Mat,
    size: usize,
}

impl CostMatrix {
    pub fn new() -> CostMatrix {
        CostMatrix {
            costs: [[0; MAX_SIZE]; MAX_SIZE],
            size: 0
        }
    }

    pub fn distance(&self, home: usize, away: usize) -> usize {
        self.costs[home][away]
    }

    pub fn num_teams(&self) -> usize {
        self.size
    }

    pub fn from(costs: Vec<Vec<usize>>) -> Result<CostMatrix, String> {
        if costs.len() > MAX_SIZE || costs[0].len() > MAX_SIZE {
            Err(format!("Costs too long"))
        } else {
            let mut m = [[0; MAX_SIZE]; MAX_SIZE];
            for i in 0..costs.len() {
                for j in 0..costs[i].len() {
                    m[i][j] = costs[i][j];
                }
            }
            Ok(CostMatrix { costs: m, size: costs.len() })
        }
    }

    pub fn from_file(name: String) -> Result<CostMatrix, Box<dyn std::error::Error>> {
        let contents = fs::read_to_string(name)?;
        let mut res = Vec::new();
        for line in contents.split("\n") {
            let mut entries: Vec<usize> = Vec::new();
            if line.is_empty() {
                continue;
            }
            for entry in line.split(" ") {
                let num = entry.parse::<usize>()?;
                entries.push(num);
            }
            res.push(entries);
        }
        Ok(CostMatrix::from(res)?)
    }
}
