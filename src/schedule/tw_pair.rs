use std::cmp::Ordering;

#[derive(Copy, Clone, Debug, Eq)]
pub struct TWPair {
    pub t: usize,
    pub w: usize,
}

impl TWPair {
    pub fn new(t: usize, w: usize) -> TWPair {
        TWPair { t, w }
    }
}

impl Ord for TWPair {
    fn cmp(&self, other: &TWPair) -> Ordering {
        match self.t.cmp(&other.t) {
            Ordering::Greater => Ordering::Greater,
            Ordering::Less => Ordering::Less,
            Ordering::Equal => self.w.cmp(&other.w),
        }
    }
}

impl PartialOrd for TWPair {
    fn partial_cmp(&self, other: &TWPair) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for TWPair {
    fn eq(&self, other: &TWPair) -> bool {
        (self.t == other.t) && (self.w == other.w)
    }
}
