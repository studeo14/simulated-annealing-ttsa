use crate::schedule::Schedule;
//use rand_xorshift::XorShiftRng as PRng;
use rand::rngs::StdRng as PRng;
use rand::Rng;

pub struct SimulatedAnnealing {
    schedule: Schedule,
    t_0: usize,
    beta: f64,
    w_0: f64,
    delta: f64,
    theta: f64,
    max_c: usize,
    max_p: usize,
    max_r: usize,
    rng: PRng,
}

impl SimulatedAnnealing {
    pub fn new(
        initial_schedule: Schedule,
        t_0: usize,
        beta: f64,
        w_0: f64,
        delta: f64,
        theta: f64,
        max_c: usize,
        max_p: usize,
        max_r: usize,
        rng: PRng,
    ) -> SimulatedAnnealing {
        SimulatedAnnealing {
            schedule: initial_schedule,
            t_0,
            beta,
            w_0,
            delta,
            theta,
            max_c,
            max_p,
            max_r,
            rng,
        }
    }

    pub fn schedule(&self) -> &Schedule {
        &self.schedule
    }

    pub fn mut_schedule(&mut self) -> &mut Schedule {
        &mut self.schedule
    }

    pub fn f(v: f64) -> f64 {
        1.0 + v.sqrt()* v.ln() / 2.0
    }

    pub fn c(schedule: &Schedule, w: f64) -> f64 {
        // check feasible
        let nbv = schedule.feasible();
        let cost = schedule.cost() as f64;
        if nbv > 0 {
            let nbv = nbv as f64;
            ((cost).powi(2) + (w * SimulatedAnnealing::f(nbv)).powi(2)).sqrt()
        } else {
            cost
        }
    }

    pub fn do_move(&mut self, values: &(usize, usize, usize, usize, usize)) -> Result<(), String> {
        // println!("{}: {} {} {} {}", move_select, team1, team2, round1, round2);
        match values.0 {
            0 => {
                // swap homes
                // need two numbers
                self.schedule.swap_homes(values.1, values.2)?;
            }
            1 => {
                // swap rounds
                // need two numbers
                self.schedule.swap_rounds(values.3, values.4)?;
            }
            2 => {
                // swap teams
                // need two numbers
                self.schedule.swap_teams(values.1, values.2)?;
            }
            3 => {
                // partial swap rounds
                // need three numbers
                self.schedule
                    .partial_swap_rounds(values.1, values.3, values.4)?;
            }
            4 => {
                // partial swap teams
                // need three numbers
                self.schedule
                    .partial_swap_teams(values.1, values.2, values.3)?;
            }
            _ => {}
        }
        Ok(())
    }

    pub fn do_random_move(&mut self) -> Result<(usize, usize, usize, usize, usize), String> {
        // need type of move
        let team1 = self.rng.gen_range(1, self.schedule.teams + 1);
        let mut team2 = self.rng.gen_range(1, self.schedule.teams + 1);
        while team1 == team2 {
            team2 = self.rng.gen_range(1, self.schedule.teams + 1);
        }
        let round1 = self.rng.gen_range(1, self.schedule.weeks + 1);
        let mut round2 = self.rng.gen_range(1, self.schedule.weeks + 1);
        while round1 == round2 {
            round2 = self.rng.gen_range(1, self.schedule.weeks + 1);
        }
        let move_select = self.rng.gen_range(0, 5);
        let values = (move_select, team1, team2, round1, round2);
        // println!("{:?}", values);
        self.do_move(&values)?;
        Ok(values)
    }

    pub fn run(&mut self) {
        let mut best_feasible = std::f64::MAX;
        let mut best_infeasible = std::f64::MAX;
        let mut best_temperature = self.t_0 as f64;
        let mut nbf = std::f64::MAX;
        let mut nbi = std::f64::MAX;
        let mut reheat = 0;
        let mut counter;
        let mut w = self.w_0;
        let mut t = self.t_0 as f64;
        self.schedule.update_cost();
        self.schedule.update_feasible();
        let mut curr_c = SimulatedAnnealing::c(&self.schedule, w);
        let mut curr_nbv;
        //
        let mut new_c;
        let mut test: f64;
        let mut prob;
        let mut dc;
        let mut new_nbv;
        let mut values;
        while reheat <= self.max_r {
            let mut phase = 0;
            while phase <= self.max_p {
                counter = 0;
                while counter <= self.max_c {
                    // get random move
                    values = match self.do_random_move() {
                        Ok(s) => s,
                        Err(_) => continue,
                    };
                    self.schedule.update_cost();
                    self.schedule.update_feasible();
                    new_nbv = self.schedule.feasible();
                    new_c = SimulatedAnnealing::c(&self.schedule, w);
                    let accept = if (new_c <= curr_c)
                        || (new_nbv == 0 && new_c < best_feasible)
                        || (new_nbv > 0 && new_c < best_infeasible)
                    {
                        true
                    } else {
                        dc = new_c - curr_c;
                        test = self.rng.gen();
                        prob = ((-1.0 * dc) / t).exp();
                        test < prob
                    };
                    if accept {
                        curr_c = new_c;
                        curr_nbv = new_nbv;
                        if curr_nbv == 0 {
                            nbf = curr_c.min(best_feasible);
                        } else {
                            nbi = curr_c.min(best_infeasible);
                        }
                        if nbf < best_feasible || nbi < best_infeasible {
                            // println!("\nGot it");
                            reheat = 0;
                            counter = 0;
                            phase = 0;
                            best_temperature = t;
                            best_feasible = nbf;
                            best_infeasible = nbi;
                            if curr_nbv == 0 {
                                w = w / self.theta;
                            } else {
                                w = w * self.delta;
                            }
                        } else {
                            counter += 1;
                        }
                    } else {
                        // undo
                        if let Err(e) =  self.do_move(&values) {
                            panic!(e);
                        }
                    }
                }
                if phase % 20 == 0 {
                    // print!("\r{:.4}, {} @ {:.4}, {:.4}, {:.4}, {:.4}, {:.4}, {:.4}, {:.4}, {:?}", curr_c, curr_nbv, t, phase, reheat, w, best_temperature, best_feasible, best_infeasible, values);
                }
                phase += 1;
                t = t * self.beta;
            }
            reheat += 1;
            t = 2.0 * best_temperature;
        }
    }
}
