use lab2;
use lab2::schedule::cost_matrix::CostMatrix;
use lab2::schedule::Schedule;
use lab2::simulated_annealing::SimulatedAnnealing;
use rand::SeedableRng;
//use rand_xorshift::XorShiftRng as PRng;
use rand::rngs::StdRng;

#[macro_use]
extern crate clap;
use clap::App;

fn main() {
    let yaml = load_yaml!("cli.yaml");
    let matches = App::from_yaml(yaml).get_matches();

    let cm = value_t!(matches, "cost_matrix", String).unwrap_or_else(|e| e.exit());
    let t0 = value_t!(matches, "t0", usize).unwrap_or_else(|e| e.exit()); let w = value_t!(matches, "w0", f64).unwrap_or_else(|e| e.exit());
    let b = value_t!(matches, "beta", f64).unwrap_or_else(|e| e.exit());
    let d = value_t!(matches, "delta", f64).unwrap_or_else(|e| e.exit());
    let th = value_t!(matches, "theta", f64).unwrap_or_else(|e| e.exit());
    let mc = value_t!(matches, "max_c", usize).unwrap_or_else(|e| e.exit());
    let mp = value_t!(matches, "max_p", usize).unwrap_or_else(|e| e.exit());
    let mr = value_t!(matches, "max_r", usize).unwrap_or_else(|e| e.exit());
    let seed = value_t!(matches, "seed", u8).unwrap_or_else(|e| e.exit());
    let seed1 = [seed; 32];
    let mut seed2 = [seed; 16];
    let cm = CostMatrix::from_file(cm).expect("Unable to read costs matrix file");
    let teams = cm.num_teams();
    //let rng = PRng::from_seed(seed1);
    let rng = StdRng::from_seed(seed1);
    println!("TTP {}", teams);
    let mut initial_schedule = Schedule::random_schedule_garunteed(teams, &mut seed2);
    initial_schedule.set_costs(cm);
    let mut sim = SimulatedAnnealing::new(
        initial_schedule,
        t0,
        b,
        w,
        d,
        th,
        mc,
        mp,
        mr,
        rng
    );
    // sim.schedule().print();
    // for _i in 0..25 {
    //     if let Err(e) = sim.do_random_move() {
    //         println!("{}",e);
    //     }
    //     sim.schedule().print();
    // }
    // return;
    // run
    sim.run();
    sim.schedule().print();
    sim.mut_schedule().update_cost();
    sim.mut_schedule().update_feasible();
    println!("Cost: {}", sim.schedule().cost());
    println!("NBV: {}", sim.schedule().feasible());
}
