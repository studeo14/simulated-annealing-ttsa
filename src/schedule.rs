pub mod cost_matrix;
pub mod game;
pub mod tw_pair;

use cost_matrix::CostMatrix;
use game::{Game, GameType};
use rand::seq::SliceRandom;
use rand::SeedableRng;
use rand_xorshift::XorShiftRng as PRng;
use tw_pair::TWPair;

use std::collections::HashSet;

pub const SEED_SIZE: usize = 16;
pub const DEFAULT_GAME: Game = Game {
    val: 0,
    game_type: GameType::Home,
};
pub type Seed = [u8; SEED_SIZE];

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Schedule {
    pub table: Vec<Vec<Game>>,
    pub weeks: usize,
    pub teams: usize,
    pub costs_status: Vec<(usize, usize, bool)>,
    costs: CostMatrix,
    current_cost: usize,
    current_nbv: usize,
}

impl Schedule {
    pub fn new(teams: usize, costs: CostMatrix) -> Schedule {
        let weeks = (2 * teams) - 2;
        Schedule {
            table: vec![vec![DEFAULT_GAME; weeks]; teams],
            weeks,
            teams,
            costs,
            costs_status: vec![(0, 0, true); teams],
            current_cost: 0,
            current_nbv: 0,
        }
    }

    pub fn set_costs(&mut self, costs: CostMatrix) {
        self.costs = costs;
    }

    pub fn print(&self) {
        // print header
        for i in 0..self.weeks + 1 {
            if i == 0 {
                print! {"     "};
            } else {
                print! {"{:<5}", i};
            }
        }
        println!("");
        // print lines
        for team in 0..self.teams {
            // print team
            print!("{:<5}", team + 1);
            for week in 0..self.weeks {
                // print game for the week for that team
                print!("{:<5}", self.table[team][week]);
            }
            println!("");
        }
    }

    // fn generate_teams_weeks_pairs(teams: usize) -> Vec<TWPair> {
    //     let weeks = (2 * teams) - 2;
    //     let mut q = Vec::new();
    //     for t in 0..teams {
    //         for w in 0..weeks {
    //             let tw = TWPair::new(t + 1, w + 1);
    //             q.push(tw);
    //         }
    //     }
    //     // sort q
    //     q.sort_unstable_by(|a, &b| b.cmp(a));
    //     q
    // }

    fn generate_random_schedule(
        s: &mut Schedule,
        rng: &mut PRng,
        used: &mut Vec<HashSet<Game>>,
    ) -> bool {
        for team in 1..s.teams + 1 {
            for week in 1..s.weeks + 1 {
                let pair = TWPair::new(team, week);
                if s.table[pair.t - 1][pair.w - 1] == DEFAULT_GAME {
                    // need choices
                    let mut choices = Vec::new();
                    for ti in 0..s.teams {
                        if s.table[ti][week - 1] == DEFAULT_GAME && (ti + 1) != pair.t {
                        // if ti != pair.t - 1 {
                            choices.push(Game::new(ti + 1, GameType::Home));
                            choices.push(Game::new(ti + 1, GameType::Away));
                        }
                    }
                    choices.shuffle(rng);
                    for choice in choices {
                        let test_pair = TWPair::new(choice.val, week);
                        if s.table[test_pair.t - 1][test_pair.w - 1] == DEFAULT_GAME
                            && !used[pair.t - 1].contains(&choice)
                        {
                            // set pair
                            s.table[pair.t - 1][pair.w - 1] = choice;
                            // set choice
                            let game = Game::new(pair.t, GameType::opposite(choice.game_type));
                            s.table[test_pair.t - 1][test_pair.w - 1] = game;
                            used[pair.t - 1].insert(choice);
                            used[test_pair.t - 1].insert(game);
                            if Schedule::generate_random_schedule(s, rng, used) {
                                return true;
                            } else {
                                // set pair
                                s.table[pair.t - 1][pair.w - 1] = DEFAULT_GAME;
                                // set choice
                                s.table[test_pair.t - 1][test_pair.w - 1] = DEFAULT_GAME;
                                //
                                used[pair.t - 1].remove(&choice);
                                used[test_pair.t - 1].remove(&game);
                            }
                        }
                    }
                    return false;
                }
            }
        }
        true
    }

    pub fn random_schedule(teams: usize, seed: &Seed) -> Option<Schedule> {
        let mut rng = PRng::from_seed(*seed);
        let mut sched = Schedule::new(teams, CostMatrix::new());
        let mut used = vec![HashSet::new(); teams];
        if Schedule::generate_random_schedule(&mut sched, &mut rng, &mut used)
            // && Schedule::generate_random_schedule(&mut sched, &mut rng, &mut used)
        {
            Some(sched)
        } else {
            None
        }
    }

    fn random_schedule_garunteed_helper(
        teams: usize,
        starting_seed: &mut Seed,
        seed_index: &mut usize,
    ) -> Schedule {
        let s = Schedule::random_schedule(teams, starting_seed).unwrap();
        for team in &s.table {
            if team.contains(&DEFAULT_GAME) {
                if starting_seed[*seed_index] >= 255 {
                    if *seed_index >= SEED_SIZE - 1 {
                        return s;
                    }
                    *seed_index += 1;
                }
                starting_seed[*seed_index] += 1;
                return Schedule::random_schedule_garunteed_helper(teams, starting_seed, seed_index);
            }
        }
        s
    }

    pub fn random_schedule_garunteed(teams: usize, starting_seed: &mut Seed) -> Schedule {
        let mut i = 0;
        Schedule::random_schedule_garunteed_helper(teams, starting_seed, &mut i)
    }

    pub fn swap_homes(&mut self, teama: usize, teamb: usize) -> Result<(), String> {
        if teama == teamb {
            Err(format!("Teams are the same"))
        } else if teama > self.teams || teamb > self.teams {
            Err(format!("Teams do not exist"))
        } else {
            // swap homes for a
            for game in &mut self.table[teama - 1] {
                if game.val == teamb {
                    *game = Game::new(game.val, GameType::opposite(game.game_type));
                }
            }
            // swap homes for b
            for game in &mut self.table[teamb - 1] {
                if game.val == teama {
                    *game = Game::new(game.val, GameType::opposite(game.game_type));
                }
            }
            self.costs_status[teama - 1].2 = true;
            self.costs_status[teamb - 1].2 = true;
            Ok(())
        }
    }

    pub fn swap_rounds(&mut self, rounda: usize, roundb: usize) -> Result<(), String> {
        if rounda == roundb {
            Err(format!("Rounds are the same"))
        } else if rounda > self.weeks || roundb > self.weeks {
            Err(format!("Rounds do not exist"))
        } else {
            // swap rounds
            let rounda = rounda - 1;
            let roundb = roundb - 1;
            for team in 0..self.teams {
                let temp = self.table[team][roundb];
                self.table[team][roundb] = self.table[team][rounda];
                self.table[team][rounda] = temp;
                self.costs_status[team].2 = true;
            }
            Ok(())
        }
    }

    pub fn swap_teams(&mut self, teama: usize, teamb: usize) -> Result<(), String> {
        if teama == teamb {
            Err(format!("Teams are the same"))
        } else if teama > self.teams || teamb > self.teams {
            Err(format!("Teams do not exist"))
        } else {
            let teama_i = teama - 1;
            let teamb_i = teamb - 1;
            self.costs_status[teama_i].2 = true;
            self.costs_status[teamb_i].2 = true;
            // now swap team a and team b games
            // go through all of teama's games
            for week in 0..self.weeks {
                // if game.val is teamb then ignore
                // else swap games with teamb's of that week
                let a_game = self.table[teama_i][week];
                if a_game.val != teamb {
                    // swap
                    self.table[teama_i][week] = self.table[teamb_i][week];
                    self.table[teamb_i][week] = a_game;
                }
            }
            // next correct opponent games
            // for a sched, set game for opponent
            for week in 0..self.weeks {
                let game_a = self.table[teama_i][week];
                let game_b = self.table[teamb_i][week];
                self.table[game_a.val - 1][week] =
                    Game::new(teama, GameType::opposite(game_a.game_type));
                self.table[game_b.val - 1][week] =
                    Game::new(teamb, GameType::opposite(game_b.game_type));
                self.costs_status[game_a.val - 1].2 = true;
                self.costs_status[game_b.val - 1].2 = true;
            }
            Ok(())
        }
    }

    fn swap_single_round(
        &mut self,
        team: usize,
        rounda: usize,
        roundb: usize,
        visited: &mut Vec<bool>,
    ) -> Result<(), String> {
        // for team swap the games for rounda and roundb
        if team > self.teams {
            Err(format!("Team does not exist"))
        } else if rounda > self.weeks || roundb > self.weeks {
            Err(format!("Rounds do not exist"))
        } else if rounda == roundb {
            Err(format!("Rounds are equal"))
        } else if visited[team - 1] {
            Ok(())
        } else {
            // println!("Swapping {}, {} for team {}", rounda, roundb, team);
            // swap current
            let team_ix = team - 1;
            let rounda_ix = rounda - 1;
            let roundb_ix = roundb - 1;
            let gamea = self.table[team_ix][rounda_ix];
            let gameb = self.table[team_ix][roundb_ix];
            visited[team_ix] = true;
            // recurse
            self.swap_single_round(gamea.val, rounda, roundb, visited)?;
            self.swap_single_round(gameb.val, rounda, roundb, visited)?;
            self.table[team_ix][rounda_ix] = gameb;
            self.table[team_ix][roundb_ix] = gamea;
            self.costs_status[team_ix].2 = true;
            Ok(())
        }
    }

    pub fn partial_swap_rounds(
        &mut self,
        team: usize,
        rounda: usize,
        roundb: usize,
    ) -> Result<(), String> {
        let mut visited = vec![false; self.teams];
        self.swap_single_round(team, rounda, roundb, &mut visited)
    }

    fn swap_single_team(
        &mut self,
        teama: usize,
        teamb: usize,
        round: usize,
        visited: &mut Vec<bool>,
    ) -> Result<(), String> {
        if round > self.weeks {
            Err(format!("Round does not exist"))
        } else if teama > self.teams || teamb > self.teams {
            Err(format!("Teams do not exist"))
        } else if teamb == teama {
            Err(format!("Teams are equal"))
        } else if visited[round - 1] {
            Ok(())
        } else {
            // println!("Swapping {}, {} for round {}", teama, teamb, round);
            // swap
            // set opponents of swaps
            // check for conflicts in teama sched
            // recurse to problem round
            let teama_ix = teama - 1;
            let teamb_ix = teamb - 1;
            let round_ix = round - 1;
            let game_a = self.table[teama_ix][round_ix];
            if game_a.val == teamb {
                return Err(format!(
                    "Teams {} and {} play each other in round {}. Cannot swap.",
                    teama, teamb, round
                ));
            }
            // swap current teams
            visited[round - 1] = true;
            let temp = self.table[teama_ix][round_ix];
            self.table[teama_ix][round_ix] = self.table[teamb_ix][round_ix];
            self.table[teamb_ix][round_ix] = temp;
            self.costs_status[teama_ix].2 = true;
            self.costs_status[teamb_ix].2 = true;
            // set opponents
            let game_a = self.table[teama_ix][round_ix];
            let game_b = self.table[teamb_ix][round_ix];
            self.table[game_a.val - 1][round_ix] =
                Game::new(teama, GameType::opposite(game_a.game_type));
            self.table[game_b.val - 1][round_ix] =
                Game::new(teamb, GameType::opposite(game_b.game_type));
            self.costs_status[game_a.val - 1].2 = true;
            self.costs_status[game_b.val - 1].2 = true;
            // look for conflicts
            for r in 0..self.weeks {
                if self.table[teama_ix][r] == game_a && (round_ix != r) {
                    // recurse
                    return self.swap_single_team(teama, teamb, r + 1, visited);
                }
            }
            Ok(())
        }
    }

    pub fn partial_swap_teams(
        &mut self,
        teama: usize,
        teamb: usize,
        round: usize,
    ) -> Result<(), String> {
        let mut visited = vec![false; self.weeks];
        self.swap_single_team(teama, teamb, round, &mut visited)
    }

    pub fn count_violations(sched: &Vec<Game>) -> usize {
        let mut nbv = 0;
        let mut num_consecutive_home = 0;
        let mut num_consecutive_away = 0;
        let mut prev_game = &DEFAULT_GAME;
        for game in sched {
            if *prev_game == DEFAULT_GAME {
                //init
                match game.game_type {
                    GameType::Home => num_consecutive_home = 1,
                    GameType::Away => num_consecutive_away = 1,
                }
            } else {
                if prev_game.game_type == game.game_type {
                    // update consecutive
                    match game.game_type {
                        GameType::Home => num_consecutive_home += 1,
                        GameType::Away => num_consecutive_away += 1,
                    }
                    // atmost
                    if num_consecutive_away >= 3 {
                        nbv += 1;
                        num_consecutive_away = 2;
                    } else if num_consecutive_home >= 3 {
                        nbv += 1;
                        num_consecutive_home = 2;
                    }
                } else {
                    // norepeat
                    if prev_game.val == game.val {
                        nbv += 1;
                    }
                    num_consecutive_away = 0;
                    num_consecutive_home = 0;
                    match game.game_type {
                        GameType::Home => num_consecutive_home = 1,
                        GameType::Away => num_consecutive_away = 1,
                    }
                }
            }
            prev_game = &game;
        }
        nbv
    }

    pub fn update_feasible(&mut self) {
        let mut nbv = 0;
        for team in 0..self.teams {
            nbv += if !self.costs_status[team].2 {
                self.costs_status[team].1
            } else {
                self.costs_status[team].1 = Schedule::count_violations(&self.table[team]);
                self.costs_status[team].1
            }
        }
        self.current_nbv = nbv;
    }
    // count # of norepeat and atmost constraints
    // none -> feasible
    pub fn feasible(&self) -> usize {
        self.current_nbv
    }

    fn single_cost(&mut self, team: usize) -> usize {
        if !self.costs_status[team].2 {
            return self.costs_status[team].0;
        }
        let mut cost = 0;
        let first_game = Game::new(team + 1, GameType::Home);
        let mut last_game = &first_game;
        for week in 0..self.weeks {
            let game = &self.table[team][week];
            match (last_game.game_type, game.game_type) {
                (GameType::Home, GameType::Away) => {
                    // println!("{},{}", team + 1, game.val);
                    cost += self.costs.distance(team, game.val - 1)
                }
                (GameType::Away, GameType::Home) => {
                    // println!("{},{}", last_game.val, team + 1);
                    cost += self.costs.distance(last_game.val - 1, team)
                }
                (GameType::Away, GameType::Away) => {
                    // println!("{},{}", last_game.val, game.val);
                    cost += self.costs.distance(last_game.val - 1, game.val - 1)
                }
                (_, _) => {},
            }
            last_game = game;
        }
        if let GameType::Away = last_game.game_type {
            // println!("{},{}", last_game.val, team + 1);
            cost += self.costs.distance(last_game.val - 1, team);
        }
        self.costs_status[team].0 = cost;
        cost
    }

    pub fn update_cost(&mut self) {
        let mut cost = 0;
        for team in 0..self.teams {
            let single_costs = self.single_cost(team);
            // println!("SC: {}", single_costs);
            cost += single_costs;
        }
        self.current_cost = cost;
    }

    pub fn cost(&self) -> usize {
        self.current_cost
    }

    pub fn is_drr(&self) -> bool {
        for team in 0..self.teams {
            let mut games = HashSet::new();
            for week in 0..self.weeks {
                // check
                let game = self.table[team][week];
                if games.contains(&game) {
                    return false;
                }
                games.insert(game);
            }
        }
        true
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn costs() {
        let cm = CostMatrix::from(vec![
                vec![0, 1, 1, 1],
                vec![1, 0, 1, 1],
                vec![1, 1, 0, 1],
                vec![1, 1, 1, 0],
            ]
        ).expect("Unable to make cm");
        let mut ss = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut ss);
        s.set_costs(cm);
        s.print();
        s.update_cost();
        assert_eq!(s.cost(), 19);
    }

    #[test]
    fn violations_count() {
        let test = vec![
            Game::new(1, GameType::Home),
            Game::new(1, GameType::Away),
            Game::new(2, GameType::Home),
            Game::new(3, GameType::Home),
        ];
        assert_eq!(1, Schedule::count_violations(&test), "1");
        let test = vec![
            Game::new(1, GameType::Away),
            Game::new(4, GameType::Home),
            Game::new(2, GameType::Home),
            Game::new(3, GameType::Home),
        ];
        assert_eq!(1, Schedule::count_violations(&test), "2");
        let test = vec![
            Game::new(1, GameType::Away),
            Game::new(1, GameType::Home),
            Game::new(2, GameType::Home),
            Game::new(3, GameType::Home),
        ];
        assert_eq!(2, Schedule::count_violations(&test), "3");
        let test = vec![
            Game::new(1, GameType::Away),
            Game::new(1, GameType::Home),
            Game::new(2, GameType::Away),
            Game::new(3, GameType::Away),
        ];
        assert_eq!(1, Schedule::count_violations(&test), "4");
        let test = vec![
            Game::new(1, GameType::Home),
            Game::new(1, GameType::Away),
            Game::new(2, GameType::Away),
            Game::new(3, GameType::Away),
        ];
        assert_eq!(2, Schedule::count_violations(&test), "5");
        let test = vec![
            Game::new(4, GameType::Home),
            Game::new(1, GameType::Home),
            Game::new(4, GameType::Away),
            Game::new(3, GameType::Away),
            Game::new(1, GameType::Away),
            Game::new(3, GameType::Home),
        ];
        assert_eq!(1, Schedule::count_violations(&test), "6");
        let test = vec![
            Game::new(4, GameType::Home),
            Game::new(1, GameType::Home),
            Game::new(4, GameType::Away),
            Game::new(3, GameType::Away),
            Game::new(1, GameType::Away),
            Game::new(3, GameType::Home),
            Game::new(4, GameType::Away),
            Game::new(3, GameType::Away),
            Game::new(1, GameType::Away),
            Game::new(4, GameType::Home),
            Game::new(1, GameType::Home),
            Game::new(1, GameType::Home),
        ];
        assert_eq!(3, Schedule::count_violations(&test), "7");
    }

    #[ignore]
    #[test]
    fn random_schedule_non_zero() {
        let mut seed = [0; 16];
        while seed[0] < 255 {
            let mut teams = 4;
            while teams < 16 {
                let mut ss = seed.clone();
                let s = Schedule::random_schedule_garunteed(teams, &mut ss);
                for team in &s.table {
                    if team.contains(&DEFAULT_GAME) {
                        println!("Seed: {:?}\nTeams: {}", seed, teams);
                        s.print();
                        assert!(false);
                    }
                }
                teams += 2;
            }
            seed[0] += 1;
        }
    }

    #[ignore]
    #[test]
    fn random_schedule_all_drr() {
        let mut seed = [0; 16];
        while seed[0] < 255 {
            let mut teams = 4;
            while teams < 16 {
                let mut ss = seed.clone();
                let s = Schedule::random_schedule_garunteed(teams, &mut ss);
                assert!(s.is_drr(), format!("{}, {:?}", teams, seed));
                teams += 2;
            }
            seed[0] += 1;
        }
    }

    #[test]
    fn swap_homes_fails() {
        let mut s = Schedule::new(4, CostMatrix::new());
        let res = s.swap_homes(1, 1);
        assert_eq!(Err(String::from("Teams are the same")), res);
        let res = s.swap_homes(10, 1);
        assert_eq!(Err(String::from("Teams do not exist")), res);
        let res = s.swap_homes(1, 10);
        assert_eq!(Err(String::from("Teams do not exist")), res);
    }

    #[test]
    fn swap_homes_basic() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut seed);
        let test = Schedule::random_schedule_garunteed(4, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_homes(pair.0, pair.1).expect("Unable to swap");
            assert!(s.is_drr());
            s.swap_homes(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_homes_large() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(10, &mut seed);
        let test = Schedule::random_schedule_garunteed(10, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_homes(pair.0, pair.1).expect("Unable to swap");
            assert!(s.is_drr(), format!("{:?}", pair));
            s.swap_homes(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_rounds_fails() {
        let mut s = Schedule::new(4, CostMatrix::new());
        let res = s.swap_rounds(1, 1);
        assert_eq!(Err(String::from("Rounds are the same")), res);
        let res = s.swap_rounds(10, 1);
        assert_eq!(Err(String::from("Rounds do not exist")), res);
        let res = s.swap_rounds(1, 10);
        assert_eq!(Err(String::from("Rounds do not exist")), res);
    }

    #[test]
    fn swap_rounds_basic() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut seed);
        let test = Schedule::random_schedule_garunteed(4, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.weeks + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.weeks + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_rounds(pair.0, pair.1).expect("Unable to swap");
            assert!(s.is_drr());
            s.swap_rounds(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_rounds_large() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(10, &mut seed);
        let test = Schedule::random_schedule_garunteed(10, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.weeks + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.weeks + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_rounds(pair.0, pair.1).expect("Unable to swap");
            s.swap_rounds(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_teams_fails() {
        let mut s = Schedule::new(4, CostMatrix::new());
        let res = s.swap_teams(1, 1);
        assert_eq!(Err(String::from("Teams are the same")), res);
        let res = s.swap_teams(10, 1);
        assert_eq!(Err(String::from("Teams do not exist")), res);
        let res = s.swap_teams(1, 10);
        assert_eq!(Err(String::from("Teams do not exist")), res);
    }

    #[test]
    fn swap_teams_basic() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut seed);
        let test = Schedule::random_schedule_garunteed(4, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_teams(pair.0, pair.1).expect("Unable to swap");
            assert!(s.is_drr());
            s.swap_teams(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_teams_large() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(10, &mut seed);
        let test = Schedule::random_schedule_garunteed(10, &mut seed);
        let swap_pairs: Vec<(usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| (team, other))
                    .collect();
                res
            })
            .flatten()
            .collect();

        for pair in swap_pairs {
            s.swap_teams(pair.0, pair.1).expect("Unable to swap");
            assert!(s.is_drr());
            s.swap_teams(pair.0, pair.1).expect("Unable to swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_teams_partial_fail() {
        let mut s = Schedule::new(4, CostMatrix::new());
        let res = s.partial_swap_teams(10, 1, 1);
        assert_eq!(Err(String::from("Teams do not exist")), res);
        let res = s.partial_swap_teams(1, 10, 1);
        assert_eq!(Err(String::from("Teams do not exist")), res);
        let res = s.partial_swap_teams(1, 1, 1);
        assert_eq!(Err(String::from("Teams are equal")), res);
        let res = s.partial_swap_teams(1, 2, 10);
        assert_eq!(Err(String::from("Round does not exist")), res);
    }

    #[test]
    fn swap_teams_partial_basic() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut seed);
        let test = Schedule::random_schedule_garunteed(4, &mut seed);
        let swap_triples: Vec<(usize, usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| {
                        let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                            .map(|round| (team, other, round))
                            .collect();
                        res
                    })
                    .flatten()
                    .collect();
                res
            })
            .flatten()
            .collect();

        for triple in swap_triples {
            if let Err(e) =  s.partial_swap_teams(triple.0, triple.1, triple.2) {
                if e.contains("play each other"){
                   continue;
                } else {
                    panic!(e);
                }
            }
            assert!(s.is_drr(), format!("{:?}", triple));
            s.partial_swap_teams(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_teams_partial_large() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(10, &mut seed);
        let test = Schedule::random_schedule_garunteed(10, &mut seed);
        let swap_triples: Vec<(usize, usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize, usize)> = (1..s.teams + 1)
                    .filter(|other| *other != team)
                    .map(|other| {
                        let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                            .map(|round| (team, other, round))
                            .collect();
                        res
                    })
                    .flatten()
                    .collect();
                res
            })
            .flatten()
            .collect();

        for triple in swap_triples {
            if let Err(e) =  s.partial_swap_teams(triple.0, triple.1, triple.2) {
                if e.contains("play each other"){
                    continue;
                } else {
                    panic!(e);
                }
            }
            assert!(s.is_drr(), format!("{:?}", triple));
            s.partial_swap_teams(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_rounds_partial_fail() {
        let mut s = Schedule::new(4, CostMatrix::new());
        let res = s.partial_swap_rounds(10, 1, 2);
        assert_eq!(Err(String::from("Team does not exist")), res);
        let res = s.partial_swap_rounds(1, 10, 1);
        assert_eq!(Err(String::from("Rounds do not exist")), res);
        let res = s.partial_swap_rounds(1, 2, 10);
        assert_eq!(Err(String::from("Rounds do not exist")), res);
        let res = s.partial_swap_rounds(1, 1, 1);
        assert_eq!(Err(String::from("Rounds are equal")), res);
    }

    #[test]
    fn swap_rounds_partial_basic() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(4, &mut seed);
        let test = Schedule::random_schedule_garunteed(4, &mut seed);
        let swap_triples: Vec<(usize, usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                    .map(|other| {
                        let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                            .filter(|round| *round != other)
                            .map(|round| (team, other, round))
                            .collect();
                        res
                    })
                    .flatten()
                    .collect();
                res
            })
            .flatten()
            .collect();

        for triple in swap_triples {
            s.partial_swap_rounds(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert!(s.is_drr());
            s.partial_swap_rounds(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert_eq!(s, test);
        }
    }

    #[test]
    fn swap_rounds_partial_large() {
        let mut seed = [0; 16];
        let mut s = Schedule::random_schedule_garunteed(10, &mut seed);
        let test = Schedule::random_schedule_garunteed(10, &mut seed);
        let swap_triples: Vec<(usize, usize, usize)> = (1..s.teams + 1)
            .map(|team| {
                let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                    .map(|other| {
                        let res: Vec<(usize, usize, usize)> = (1..s.weeks + 1)
                            .filter(|round| *round != other)
                            .map(|round| (team, other, round))
                            .collect();
                        res
                    })
                    .flatten()
                    .collect();
                res
            })
            .flatten()
            .collect();

        for triple in swap_triples {
            s.partial_swap_rounds(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert!(s.is_drr());
            s.partial_swap_rounds(triple.0, triple.1, triple.2).expect("Unable to partial swap");
            assert_eq!(s, test);
        }
    }
}
