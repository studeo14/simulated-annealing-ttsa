# README #

TTSA Implementation in Rust

## Running ##

Run `cargo build --release`
Run `cargo run --release -- --help` to see a list of options
or
Run `default_runner.sh -s <seed> --cost_matrix=<path>`

## Cost matricies ##

Located in `res/data<teams>.txt`